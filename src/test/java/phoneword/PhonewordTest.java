package phoneword;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class PhonewordTest {

    @Test
    public void test_getWordsByNumbers_shouldReturnWords() {

        Phoneword phoneword = new Phoneword(Collections.singletonList("872.798466"),
                new HashSet<>(Arrays.asList("USA", "PYTHON")));

        Map<Integer, Set<String>> result = phoneword.getWordsByNumbers();

        Assertions.assertEquals("USA-PYTHON", result.values().iterator().next().iterator().next().toUpperCase());
    }

    @Test
    public void test_getMatchedWordsBySlipNumberAnDictionary_shouldReturnMatchWord() {

        Phoneword phoneword = new Phoneword(new ArrayList<String>(), new HashSet<String>());

        Stack<Integer> stack = new Stack<>();
        stack.push(2);
        stack.push(7);
        stack.push(8);

        Set<String> result = phoneword.getMatchedWordsBySlipNumberAnDictionary(stack, new HashSet<>(Collections.singletonList("usa")));

        Assertions.assertEquals(1, result.size());
        Assertions.assertEquals("usa", result.iterator().next());
    }

    @Test
    public void test_getMatchedWordsBySlipNumberAnDictionary_shouldReturnNoMatchWord() {

        Phoneword phoneword = new Phoneword(new ArrayList<String>(), new HashSet<String>());

        Stack<Integer> stack = new Stack<>();
        stack.push(2);
        stack.push(7);
        stack.push(8);

        Set<String> result = phoneword.getMatchedWordsBySlipNumberAnDictionary(stack, new HashSet<>(Collections.singletonList("use")));

        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void test_buildStackNumber_shouldReturnStackWithDigit() {

        Phoneword phoneword = new Phoneword(new ArrayList<String>(), new HashSet<String>());

        Integer phone1 = 3231230;
        Stack<Integer> stack1 = new Stack<Integer>();

        phoneword.buildStackNumber(phone1, stack1);

        Assertions.assertEquals(3, stack1.pop());
        Assertions.assertEquals(2, stack1.pop());
        Assertions.assertEquals(3, stack1.pop());
        Assertions.assertEquals(1, stack1.pop());
        Assertions.assertEquals(2, stack1.pop());
        Assertions.assertEquals(3, stack1.pop());
        Assertions.assertEquals(0, stack1.pop());
    }

    @Test
    public void test_getPermutations_shouldReturnPermutationsByWords() {

        Phoneword phoneword = new Phoneword(new ArrayList<String>(), new HashSet<String>());

        List<String> result = phoneword.getPermutations(Arrays.asList("ad", "ac"), 2);

        List<String> expected = Arrays.asList("ada", "adb", "adc", "aca", "acb", "acc");

        Assertions.assertEquals(expected.size(), result.size());
        Assertions.assertTrue(result.containsAll(expected));
    }

    @Test
    public void test_addWordPrefix_shouldReturnWordLinkWithMiddleDash() {

        Phoneword phoneword = new Phoneword(new ArrayList<String>(), new HashSet<String>());

        Set<String> result1 = phoneword.addWordPrefix("Hello", new HashSet<>(Collections.singletonList("Bye")));
        Assertions.assertEquals(1, result1.size());
        Assertions.assertEquals("Hello-Bye", result1.iterator().next());

        Set<String> result2 = phoneword.addWordPrefix("Hello", new HashSet<>(Arrays.asList("Bye", "Thanks")));
        Assertions.assertEquals(2, result2.size());
        Assertions.assertTrue(result2.containsAll(new HashSet<>(Arrays.asList("Hello-Bye", "Hello-Thanks"))));
    }
}
