package phoneword;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args)  {
        List<String> phones = readLinesFromPath(args[0]);
        Set<String> dictionary = new HashSet<>(readLinesFromPath(args[1]));

        Phoneword phoneword = new Phoneword(phones, dictionary);

        Map<Integer, Set<String>> result = phoneword.getWordsByNumbers();
        print(result);
    }

    private static List<String>readLinesFromPath(String path) {
        try {
            return Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            throw new RuntimeException("Error in get File with path: " + path);
        }
    }

    private static void print(Map<Integer, Set<String>> result) {
        result.forEach((key, value) ->
            System.out.println("Number: "+ key +
                    "\nWords: " + value.stream().map(String::toUpperCase).collect(Collectors.toSet()))
        );
    }
}
