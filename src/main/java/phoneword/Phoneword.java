package phoneword;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

public class Phoneword {

    private static final Map<Integer, List<Character>> encoding = new HashMap<Integer, List<Character>>() {{
        put(2, Arrays.asList('a', 'b', 'c'));
        put(3, Arrays.asList('d', 'e', 'f'));
        put(4, Arrays.asList('g', 'h', 'i'));
        put(5, Arrays.asList('j', 'k', 'l'));
        put(6, Arrays.asList('m', 'n', 'o'));
        put(7, Arrays.asList('p', 'q', 'r', 's'));
        put(8, Arrays.asList('t', 'u', 'v'));
        put(9, Arrays.asList('w', 'x', 'y', 'z'));
    }};

    private List<String> phones;
    private Set<String> dictionary;

    public Phoneword(List<String> phones, Set<String> dictionary) {
        this.phones = phones;
        this.dictionary = dictionary;
    }

    public Map<Integer, Set<String>> getWordsByNumbers() {
        List<Integer> numbers = phones.stream()
                .map(phone -> Integer.valueOf(phone.replaceAll("[^0-9]", "")))
                .collect(Collectors.toList());

        Set<String> words = dictionary.stream()
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase())
                .collect(Collectors.toSet());

        return numbers.stream()
                .collect(Collectors.toMap(numberKey -> numberKey, number -> {
                    Stack<Integer> pipNumbers = new Stack<>();
                    buildStackNumber(number, pipNumbers);

                    return getMatchedWordsBySlipNumberAnDictionary(pipNumbers, words);
                }));
    }

    Set<String> getMatchedWordsBySlipNumberAnDictionary(Stack<Integer> pipNumbers, Set<String> dictionary){

        Set<String> words = new HashSet<>();

        List<String> permutations  = getCharsByDigit(pipNumbers.pop()).stream()
                .map(Object::toString)
                .collect(Collectors.toList());

        while(pipNumbers.size() > 0) {
            permutations = getPermutations(permutations, pipNumbers.pop());

            permutations.forEach(word -> {
                if (dictionary.contains(word)) {
                    if(pipNumbers.size() == 0) {
                        words.add(word);
                    } else {
                        Set<String> nextWords = getMatchedWordsBySlipNumberAnDictionary((Stack<Integer>) pipNumbers.clone(), dictionary);
                        words.addAll(addWordPrefix(word, nextWords));
                    }
                }
            });
        }

        return words;
    }

    Set<String> addWordPrefix(String word, Set<String> nextWords) {
        return nextWords.stream()
                .map(nextWord -> word + "-" + nextWord)
                .collect(Collectors.toSet());
    }

    void buildStackNumber(Integer number, Stack<Integer> stack) {

        Integer lastDigit = number % 10;
        Integer resultDiv = number/10;

        stack.push(lastDigit);

        if(resultDiv > 0) {
            buildStackNumber(resultDiv, stack);
        }
    }

    List<String> getPermutations(List<String> permutations, Integer digit) {
        return getCharsByDigit(digit).stream()
                .map(character -> permutations.stream()
                        .map(p -> p + character)
                        .collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<Character> getCharsByDigit(Integer digit) {
        return encoding.getOrDefault(digit, Collections.singletonList(Character.forDigit(digit, 10)));
    }
}
