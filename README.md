## PHONEWORD

### Requirements
- Java 8
- Gradle (For build and import Junit)

### How to run?
In executable files, you will find a "phoneword.jar" that you could run with the following command:

```
$ java -jar <PATH-JAR>/phoneword.jar <PATH_WITH_TXT_PHONES>/example/phones.txt <PATH_WITH_TXT_DICTIONARY>/example/dictionary.txt
```

For example:

```
java -jar executable/phoneword.jar /Users/my-user/Documents/phoneword/example/phones.txt /Users/my-user/Documents/phoneword/example/dictionary.txt
```
In the file "example", you will find the files with the proposed inputs. 

### If you need it, you can create a jar file.
```
$ ./gradlew jar
```

### Run test
```
$ ./gradlew test
```
### Technologies
- Java 1.8
- Junit 5